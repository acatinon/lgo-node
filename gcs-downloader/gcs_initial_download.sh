#!/usr/bin/env bash
set -e

ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd)"
source "${ROOT}/common/env.sh"
source "${ROOT}/common/gcs_common.sh"
source "${ROOT}/common/gcs_check_and_repair.sh"

main() {
    create_dest_directory
    CURRENT_BATCH=0
    check_and_repair
    download_new_batch_directories
}

main