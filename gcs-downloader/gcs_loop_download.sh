#!/usr/bin/env bash
set -e

ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd)"
source "${ROOT}/common/env.sh"
source "${ROOT}/common/gcs_common.sh"
source "${ROOT}/common/gcs_check_and_repair.sh"
source "${ROOT}/common/gcs_archives_download.sh"

main() {
    CURRENT_BATCH=$(cat "$CHECKPOINT" || echo 0)
    echo Starting from $CURRENT_BATCH

    if [[ -d "$DEST" && -d "$DEST/0000000000" ]]; then
        check_and_repair
        _save_checkpoint
    else
        create_dest_directory
        archives_download
        retrieve_actual_batch_id
        _save_checkpoint
    fi

    download_new_batch_directories

    CURRENT_BATCH=$(cat "$CHECKPOINT")
    check_and_repair
    _save_checkpoint

    while [[ : ]]; do
        check_or_download || sleep 5
    done

}

_save_checkpoint() {
    echo ${CURRENT_BATCH} >| "$CHECKPOINT"
}
main