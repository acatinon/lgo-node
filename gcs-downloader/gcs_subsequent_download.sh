#!/usr/bin/env bash
set -e

ROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd)"
source "${ROOT}/common/env.sh"
source "${ROOT}/common/gcs_common.sh"

main() {
    CURRENT_BATCH=0
    retrieve_actual_batch_id
    download_new_batch_directories
}

main