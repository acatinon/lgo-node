
padded_batch_id() {
    printf "%010d" ${CURRENT_BATCH}
}

download() {
    local origin=$1
    echo "Download $origin"
    gsutil -m -q cp -r "${origin}" "${DEST}"
}

create_dest_directory() {
    [[ -d "$DEST" ]] || mkdir -p "$DEST"
}

download_new_batch_directories() {
    echo -e "\nDownloading new files..."

    # Download one by one until 10 multiple
    while [[ $(($CURRENT_BATCH % 10)) -ne 0 ]]; do
      local padded=$(padded_batch_id)
      download "${BUCKET}/${padded}" || return 0
      CURRENT_BATCH=$(($CURRENT_BATCH+1))
    done
    # Download 10 by 10 until 100 multiple
    while [[ $(($CURRENT_BATCH % 100)) -ne 0 ]]; do
      local padded=$(padded_batch_id)
      download "${BUCKET}/${padded%?}*" || return 0
      CURRENT_BATCH=$(($CURRENT_BATCH+10))
    done
    # Download 100 by 100
    while : ; do
      local padded=$(padded_batch_id)
      download "${BUCKET}/${padded%??}*" || return 0
      CURRENT_BATCH=$(($CURRENT_BATCH+100))
    done
}

retrieve_actual_batch_id() {
    local max_existing_dir=$(ls -f -1 "${DEST}" | sort -r | head -1)
    local max_existing_batch=$(echo "${max_existing_dir}" | sed 's/^0*//')
    if [[ -z "$max_existing_batch" ]]; then
      echo -e "You should first use gcs_initial_download.sh"
      exit 1
    fi
    CURRENT_BATCH=${max_existing_batch}
}