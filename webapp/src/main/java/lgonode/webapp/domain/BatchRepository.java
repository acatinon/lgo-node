package lgonode.webapp.domain;

import java.util.List;

public interface BatchRepository {
    Metadata find(long id);

    List<Metadata> findAll(int requestedPage, int limit);
}
