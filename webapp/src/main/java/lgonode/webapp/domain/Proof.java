package lgonode.webapp.domain;

public class Proof {

    public Proof(long batchId, String timestampedHash, byte[] content) {
        this.batchId = batchId;
        this.timestampedHash = timestampedHash;
        this.content = content;
    }

    public final long batchId;
    public final String timestampedHash;
    public final byte[] content;
}
