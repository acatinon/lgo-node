package lgonode.webapp.domain;

public class CancelOrder {

    public CancelOrder(long id, long batchId, String keyId, long creationDate, long orderIdToCancel) {
        this.id = id;
        this.batchId = batchId;
        this.keyId = keyId;
        this.orderIdToCancel = orderIdToCancel;
        this.creationDate = creationDate;
    }

    public final long id;
    public final long batchId;
    public final String keyId;
    public final long orderIdToCancel;
    public final long creationDate;
}
