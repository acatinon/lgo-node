package lgonode.webapp.domain;

import lgonode.webapp.dao.OrderDAO;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderJooqRepository implements OrderRepository {

    @Autowired
    public OrderJooqRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<Order> findByBatchId(long batchId) {
        return dslContext.select(OrderDAO.ID, OrderDAO.BATCH_ID, OrderDAO.KEY_ID, OrderDAO.CREATION_DATE, OrderDAO.TYPE, OrderDAO.DIRECTION, OrderDAO.BASE_CURRENCY, OrderDAO.QUOTE_CURRENCY, OrderDAO.QUANTITY, OrderDAO.PRICE)
                .from(OrderDAO.TABLE)
                .where(OrderDAO.BATCH_ID.eq(batchId))
                .fetchInto(Order.class);
    }

    private final DSLContext dslContext;
}
