package lgonode.webapp.domain;

public class Metadata {

    public Metadata(long id, String hash, String previousHash, int orderCount, boolean cancelOrderBooks) {
        this.id = id;
        this.hash = hash;
        this.previousHash = previousHash;
        this.orderCount = orderCount;
        this.cancelOrderBooks = cancelOrderBooks;
    }

    public final long id;
    public final String hash;
    public final String previousHash;
    public final int orderCount;
    public final boolean cancelOrderBooks;
}
