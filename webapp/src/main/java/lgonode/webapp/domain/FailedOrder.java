package lgonode.webapp.domain;

public class FailedOrder {

    public FailedOrder(long id, long batchId, String reason) {
        this.id = id;
        this.batchId = batchId;
        this.reason = reason;
    }

    public final long id;
    public final long batchId;
    public final String reason;
}
