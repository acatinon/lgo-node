package lgonode.webapp.domain;

import lgonode.webapp.dao.BatchDAO;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BatchJooqRepository implements BatchRepository {

    @Autowired
    public BatchJooqRepository(DSLContext dslContext) {
        this.dslContext = dslContext;
    }

    @Override
    public List<Metadata> findAll(int requestedPage, int limit) {
        return dslContext.select(BatchDAO.ID, BatchDAO.HASH, BatchDAO.PREVIOUS_HASH, BatchDAO.ORDER_COUNT, BatchDAO.CANCEL_ORDER_BOOKS)
                .from(BatchDAO.TABLE)
                .orderBy(BatchDAO.ID.desc())
                .offset((requestedPage - 1) * limit)
                .limit(limit)
                .fetchInto(Metadata.class);
    }

    @Override
    public Metadata find(long id) {
        return dslContext.select(BatchDAO.ID, BatchDAO.HASH, BatchDAO.PREVIOUS_HASH, BatchDAO.ORDER_COUNT, BatchDAO.CANCEL_ORDER_BOOKS)
                .from(BatchDAO.TABLE)
                .where(BatchDAO.ID.eq(id))
                .fetchOneInto(Metadata.class);
    }

    private final DSLContext dslContext;
}
