package lgonode.webapp.controller;

import lgonode.webapp.view.BatchViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/batches/{batchId}")
public class BatchController {

    @Autowired
    public BatchController(BatchViewService batchViewService) {
        this.batchViewService = batchViewService;
    }

    @GetMapping
    public String fetchOne(Model model, @PathVariable(value = "batchId") long batchId) {
        model.addAttribute("batch", batchViewService.retrieve(batchId));
        return "batch";
    }

    private final BatchViewService batchViewService;

}
