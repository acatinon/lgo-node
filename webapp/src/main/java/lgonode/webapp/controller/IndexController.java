package lgonode.webapp.controller;

import com.google.common.io.Resources;
import com.vladsch.flexmark.ext.tables.TablesExtension;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.options.MutableDataSet;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/")
public class IndexController {

    public IndexController() {
        MutableDataSet options = new MutableDataSet().set(Parser.EXTENSIONS, Arrays.asList(TablesExtension.create()));
        Parser parser = Parser.builder(options).build();
        HtmlRenderer renderer = HtmlRenderer.builder(options).build();

        try {
            documentation.put("overview", getContent(parser, renderer, "documentation/overview.md"));
            documentation.put("config", getContent(parser, renderer, "documentation/config.md"));
            documentation.put("batching", getContent(parser, renderer, "documentation/batching.md"));
            documentation.put("details", getContent(parser, renderer, "documentation/details.md"));
            documentation.put("fairness", getContent(parser, renderer, "documentation/fairness.md"));
            documentation.put("faq", getContent(parser, renderer, "documentation/faq.md"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getContent(Parser parser, HtmlRenderer renderer, String resourceName) throws IOException {
        String content = new String(Resources.asByteSource(Resources.getResource(resourceName)).read());
        Node document = parser.parse(content);
        return renderer.render(document);
    }

    @GetMapping
    public String index(Model model) {
        for (Map.Entry<String, String> docEntry : documentation.entrySet()) {
            model.addAttribute(docEntry.getKey(), docEntry.getValue());
        }
        return "index";
    }

    private Map<String, String> documentation = new HashMap<>();

}
