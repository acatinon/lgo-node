package lgonode.webapp.dao;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.impl.DSL;

public class ProofDAO {
    public static Table<Record> TABLE = DSL.table("proof_view");
    public static Field<Long> BATCH_ID = DSL.field("batch_id", Long.class);
    public static Field<String> TIMESTAMPED_HASH = DSL.field("timestamped_hash", String.class);
    public static Field<byte[]> CONTENT = DSL.field("content", byte[].class);
}
