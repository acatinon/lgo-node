package lgonode.webapp.dao;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.impl.DSL;

public class BatchDAO {
    public static Table<Record> TABLE = DSL.table("batch_view");
    public static Field<Long> ID = DSL.field("id", Long.class);
    public static Field<String> HASH = DSL.field("hash", String.class);
    public static Field<String> PREVIOUS_HASH = DSL.field("previous_hash", String.class);
    public static Field<Integer> ORDER_COUNT = DSL.field("order_count", Integer.class);
    public static Field<Boolean> CANCEL_ORDER_BOOKS = DSL.field("cancel_order_books", Boolean.class);
}
