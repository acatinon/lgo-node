package lgonode.webapp.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
public class FairnessAppConfiguration {

    @Bean(name = "date")
    public DateFormatService dateFormatter() {
        return date -> date == null ? "" : dateFormat.format(date);
    }

    public interface DateFormatService {
        String format(Date date);
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

}