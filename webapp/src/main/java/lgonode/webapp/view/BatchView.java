package lgonode.webapp.view;

import java.util.List;

public class BatchView {

    public BatchView(long id, String hash, String previousHash, int orderCount, boolean cancelOrderBooks, List<OrderView> orderViews, ProofView proofView) {
        this.id = id;
        this.hash = hash;
        this.previousHash = previousHash;
        this.orderCount = orderCount;
        this.cancelOrderBooks = cancelOrderBooks;
        this.orderViews = orderViews;
        this.proofView = proofView;
    }

    public final long id;
    public final String hash;
    public final String previousHash;
    public final int orderCount;
    public final boolean cancelOrderBooks;
    public final List<OrderView> orderViews;
    public final ProofView proofView;
}
