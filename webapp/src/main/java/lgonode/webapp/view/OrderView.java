package lgonode.webapp.view;

import lgonode.webapp.domain.CancelOrder;
import lgonode.webapp.domain.FailedOrder;
import lgonode.webapp.domain.Order;

import java.util.Date;
import java.util.Objects;

public class OrderView implements Comparable<OrderView> {

    public OrderView(Order order) {
        id = order.id;
        keyId = order.keyId;
        creationDate = new Date(order.creationDate);
        description = marketOrder(order) ? formatMarketOrder(order) : formatLimitOrder(order);
        status = "Executed";
    }

    public OrderView(CancelOrder cancelOrder) {
        id = cancelOrder.id;
        keyId = cancelOrder.keyId;
        creationDate = new Date(cancelOrder.creationDate);
        description = "Cancel request for order " + cancelOrder.orderIdToCancel;
        status = "Executed";
    }

    public OrderView(FailedOrder failedOrder) {
        id = failedOrder.id;
        status = "Failed: " + failedOrder.reason;
    }

    public void failed(FailedOrder failedOrder) {
        status = "Failed: " + failedOrder.reason;
    }

    private String formatLimitOrder(Order order) {
        return String.format("%s Limit order of %s %s at %s %s", formatDirection(order.direction), order.quantity, order.baseCurrency, order.price, order.quoteCurrency);
    }

    private String formatMarketOrder(Order order) {
        return String.format("%s Market order of %s %s", formatDirection(order.direction), order.quantity, formatMarketCurrency(order));
    }

    private String formatDirection(String direction) {
        return direction.equals("S") ? "SELL" : "BUY";
    }

    private String formatMarketCurrency(Order order) {
        return order.direction.equals("B") ? order.quoteCurrency : order.baseCurrency;
    }

    private boolean marketOrder(Order order) {
        return order.type.equals("M");
    }

    @Override
    public int compareTo(OrderView o) {
        return Long.compare(id, o.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderView orderView = (OrderView) o;
        return id == orderView.id &&
                Objects.equals(keyId, orderView.keyId) &&
                Objects.equals(creationDate, orderView.creationDate) &&
                Objects.equals(description, orderView.description) &&
                Objects.equals(status, orderView.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, keyId, creationDate, description, status);
    }

    public long id;
    public String keyId;
    public Date creationDate;
    public String description;
    public String status;
}
