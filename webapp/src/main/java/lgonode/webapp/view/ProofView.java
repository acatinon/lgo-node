package lgonode.webapp.view;

import com.google.common.io.BaseEncoding;

public class ProofView {

    public ProofView(String timestampedHash, byte[] proof) {
        this.timestampedHash = timestampedHash;
        this.proof = BaseEncoding.base16().encode(proof);
    }

    public String timestampedHash;
    public String proof;
}
