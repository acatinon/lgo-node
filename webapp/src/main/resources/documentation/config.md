### LGO platform configuration

#### Order checks 

- quantity : (in base currency for a limit order and a market sell order, in quote currency for a market buy order)

  min ≤ remaining quantity ≤ max

- price (quote currency for limit order)

  min ≤ price ≤ max

- amount (price × remaining quantity, quote currency)

  min ≤ amount ≤ max

- price increment

  price is a multiple of quote increment
  
Market orders are only checked on quantity.

Actual limits are, for BTC/USD pair:

| Limit               | Value        |
|---------------------|--------------|
| BTC min             | BTC 0.001    |
| BTC max             | BTC 1000     |
| USD min             | USD 10       |
| USD max             | USD 1000000  |
| USD price increment | USD 0.10     |
| Amount min          | USD 10       |
| Amount max          | USD 50000000 |

#### Algorithms

- Actual Self Trade Prevention behavior: cancel remaining quantity of resting order
- Actual Matching algorithm: FIFO