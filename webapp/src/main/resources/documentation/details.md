

### LGO orders processing

#### Orders reception and decryption

The general steps and their associated order invalidity reasons before batch execution are:

1. Receive the order.
1. Verify that the key identifier is not blank (*INVALID_KEY_ID*).
1. Verify the order payload (*INVALID_ORDER_PAYLOAD*).
1. Verify the order signature (*INVALID_SIGNATURE*).
1. When the batch is ready, compute the hash of the batch content and timestamp the hash with **OpenTimestamps**.
1. Publish the batch metadata, the encrypted orders and the OpenTimestamps proof in a public bucket.
1. Request decryption from the CA.
1. If public key was not valid, add *INVALID_KEY* to failed orders.
1. Check the payload, if invalid : *INVALID_PAYLOAD*.
1. When the timestamp transaction is validated on the Bitcoin blockchain, the OpenTimestamps proof is upgraded

#### Order execution

After the orders are decrypted and their content is checked, try to execute them, in order.

1. Start batch execution

1. Was the cancellation of all orders in order books requested ? (Information available as a boolean in the metadata file). If yes, all resting orders of all order books are canceled.

1. Place all cancel orders
    - For each cancel order, try to cancel a corresponding resting order.
    - If order to cancel not from the same trader, add *NOT_OWNER_OF_ORDER_TO_CANCEL* in cancelled execution list (will be published in failed orders Avro file)
    - If order not found in books, save cancel order for in-batch cancellation.

1. Place all orders
    - For each order, one by one (ordered execution), try to execute order.
        1. Verify if it should be canceled. If yes, verify if cancel order is from same trader, if not, add the cancel order and the reason *NOT_OWNER_OF_ORDER_TO_CANCEL* in cancelled execution list
        
        2. Pre-trade securities
            - Check product : order pair should be a valid LGO product (reason: *INVALID_PRODUCT*)
            - Check limits : 
                - Market order: check quantity (*INVALID_QUANTITY*)
                - Limit order: check quantity (*INVALID_QUANTITY*), price (*INVALID_PRICE*), amount (*INVALID_AMOUNT*), price increment (*INVALID_PRICE_INCREMENT*)
            - Check funds : trader should own order amount plus taker fees amount (for BUY order) (*INSUFFICIENT_FUNDS*)
              
              Stop at the first check failing, add the order id and the failure reason to cancelled execution list
              
        3. Try to match order with right order book side
            - Loop:
                - Get top resting order
                - if cannot match, **stop**
                - if is a self trade match, execute STP algorithm and continue
                - create trade, cancel resting and taker orders if remaining to low, dequeue resting order if no remaining
                - if taker order has been entirely matched, **stop**
            - If order has no remaining quantity and is a limit order, enter order book. 

1. End batch
    - Publish cancelled executions list



