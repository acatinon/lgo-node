### Verify our fairness

To verify our fairness, you should, for each batch:

1. Verify the OpenTimestamps proof
2. Read the Avro content of orders and failed orders
3. Verify the hash of the batch
4. Verify that the hash of the previous batch is the same as your computation of the previous batch hash
5. Get the private keys of the batch orders : __gs://lgo-markets-keys/*theOrderKeyId*__
6. Decipher the orders data
7. Parse the order content
8. Execute your matching algorithm

#### Matching algorithm for the public data

After deciphering the orders and reading the failed orders details; 

For each order in the same order as found in the Avro file:
- verify if the order is present in the cancelled execution list (except with a Canceled by STP reason), if so, **stop**
- **loop**
    - verify is the top order of the corresponding order book side is in the cancelled execution list with details containing __counterOrderId:*the id of the currently executed order*__. If so, remove top resting order from book (it was cancelled by STP)
    - match the order with the top order of the corresponding order book side, if the price matches (see note below)
    - if the top resting order is entirely matched or has a remaining quantity lower than limit, remove it from the book side
    - if the executed order is entirely matched or has a remaining quantity lower than limit, **stop**
    - if the price does not match the top resting order, **break**

- if the order is a market order, **stop**
- if the order is a limit order and has sufficient remaining quantity, add it to the right book side at the end of its price level

NB: 
- A market order always matches the top resting order.
- For a sell limit order, order price should be ≤ to the resting order price.
- For a buy limit order, order price should be ≥ to the resting order price.

When two orders match, the trade is made with the resting order price.


### Detailed specifications

#### Example of batch hash calculation in JAVA:

    import com.google.common.base.Charsets;
    import com.google.common.hash.Hasher;
    import com.google.common.hash.Hashing;
    
    public String batchHash(List<EncryptedOrder> encryptedOrders, String lastBatchHash) {
        String ordersHash = ordersHash(encryptedOrders);
        return Hashing.sha256().newHasher()
                .putString(lastBatchHash == null ? ordersHash : lastBatchHash, Charsets.UTF_8)
                .putString(ordersHash, Charsets.UTF_8)
                .hash().toString();
    }
    
    private String ordersHash(List<EncryptedOrder> encryptedOrders) {
        Hasher hasher = Hashing.sha256().newHasher();
        for (EncryptedOrder encryptedOrder : encryptedOrders) {
            String hash = Hashing.sha256().newHasher()
                    .putLong(encryptedOrder.id)
                    .putString(encryptedOrder.keyId, Charsets.UTF_8)
                    .putBytes(encryptedOrder.data).hash().toString();
            hasher.putString(hash, Charsets.UTF_8);
        }
        return hasher.hash().toString();
    }



#### Public data matching algorithm

*Order books*: All order books. One order book by product (currency pair), each book has two sides (buy and sell).

*Buy side*: Treemap of long (price) and Linked list (resting orders FIFO), keys ordered in opposite order

*Sell side*: Treemap of long (price) and Linked list (resting orders FIFO), keys ordered in natural order

*Orders*: List or deciphered orders

*Failed orders*: Map of long (order id) and failed orders details

*Cancel orders*: A list of order id to cancel, built from the cancel order requests that did not fail

    if metadata.cancelOrderBooks then empty all order books sides
    
    for each cancel order, apply on orders books
    
    for each takerOrder in orders
        if takerOrder could not be executed (1)
            continue
        if takerOrder was canceled
            continue
        book ← orderBooks[takerOrder.base, takerOrder.quote]
        orderBookSide ← takerOrder.direction is BUY ? book.sellSide : book.buySide
        while takerOrder has sufficient remaining
            restingOrder ← orderBookSide.first.head
            if restingOrder was canceled by STP by takerOrder (2)
                remove restingOrder from orderBookSide
                continue
            if price does not match between takerOrder and restingOrder (5)
                break
            consume takerOrder and restingOrder to the max possible quantity
            if restingOrder has not sufficient remaining (3)
                remove restingOrder from orderBookSide
        if takerOrder has sufficient remaining (3) and is a limit order
            add takerOrder to the order book (buy side for buy order, sell side for sell order)
		 

(1)
takerOrder could not be executed :

    failedOrders contains takerOrderId 
        and failedOrder[takerOrderId].reason ≠ "OrderCanceledBySTP"

(2)
restingOrder was canceled by STP by takerOrder :

    failedOrders contains restingOrderId 
        and failedOrders[restingOrderId].reason = "OrderCanceledBySTP" 
        and failedOrders[restingOrderId].details["counterOrderId"]="takerOrderId"

(3)
order has sufficient remaining :
    
    order.remainingQuantity > 0 and limits are valid (4)

(4)
    limits are valid :

- for a market order buy:

        USD min ≤ remaining quantity ≤ USD max
    
- for a market order sell:

        BTC min ≤ remaining quantity ≤ BTC max

- for a limit order :

        BTC min ≤ remaining quantity ≤ BTC max
        USD min ≤ price ≤ USD max
        USD amount min ≤ remaining quantity × price ≤ USD amount max
        price % USD price increment = 0

(5) 
    price matches between an order and a resting order

- for a market order
    
        true
    
- for a sell limit order
    
        order.price ≤ restingOrder.price
    
- for a buy limit order
    
        order.price ≥ restingOrder.price
    
