### Overview of the solution

The process used by the LGO platform is:
- The Certificate Authority publishes public keys that have a validity period
- The traders use these public keys to cipher their orders before sending them to LGO
- LGO builds batches with received orders, publishes the hash of the batch in OpenTimestamp, and publishes the encrypted orders in a bucket
- LGO requests the Certificate Authority to decipher the batch content, given that the OpenTimestamp proof is published
- LGO executes the orders of the batch
- LGO publishes the orders that were not executable and the reason of failure
- When the public keys expire, their corresponding private keys are released by the CA