### FAQ

#### Why is the transparency tool not exactly up to date with the published batches?

First, in order to decipher the orders without asking for the CA intervention, the private keys need to be released
by the CA, which can not happen during the validity period of the public key.

Secondly, LGO node waits for the validation of the timestamp transaction on the Bitcoin blockchain. Depending on Bitcoin mempool state, it can take between 10 minutes up to 4 hours.

#### What is the tLGO1/tLGO2 product?

The tLGO1/tLGO2 product is a test product only used by the technical team to check that the platform is up and the trading is possible.