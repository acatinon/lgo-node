LGO Fairness tools 

## Setup

You need `docker` and `docker-compose` to launch the projects as containers.

If you want to launch the modules outside Docker, you need at least `java 11` and `gsutil` to use the scripts in the `gcs-download` module.

## Usage with already built Docker images

```
docker-compose -f docker-compose-no-build.yml up
```

## Usage with Docker

### Build and run

```
./gradlew dockerBuild composeUp
```

You can then visit [the webapp on port 8080](http://localhost:8080)

### Cleanup

To cleanup all data (it will remove all data in database & cache files)

```
./gradlew composeDown servicesDown
rm -rf cache/
```

## Usage without Docker

You should have the same value for the batch directory configuration in the `node` and `gcs-downloader` projects in 
`gcs-downloader/common/env.sh` and `node/src/main/resources/config/app.yaml`.

### Usage of `gcs-downloader` module

To continuously download the batches, use `gcs_loop_download.sh` (we encourage you to use this script only, it's faster and more tested).

### Usage of `node` module

`node` is a Java/Gradle project.

You can run the application with 

```
cd node/
./gradlew servicesStart run
```

### Usage of `webapp` module

`webapp` is a Java/Gradle/Spring Boot project.

You can run the application with :

```
cd webapp/
./gradlew servicesStart bootRun
```

You can then visit [the Web application on port 8080](http://localhost:8080)

## License

[MIT](LICENSE)