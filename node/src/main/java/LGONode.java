import com.google.inject.*;
import lgonode.BatchesProcessingExecutor;
import lgonode.configuration.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LGONode {

    public static void main(String[] args) {
        try {
            start();
        } catch (CreationException e) {
            e.getErrorMessages().stream().forEach(x -> LOGGER.error("Startup error", x));
        } catch (ConfigurationException e) {
            e.getErrorMessages().stream().forEach(x -> LOGGER.error("Startup error", x));
        } catch (Exception e) {
            if (e.getCause() != null) {
                LOGGER.error("Startup error: " + e.getCause().getMessage(), e);
            } else {
                LOGGER.error("Startup error: " + e.getMessage(), e);
            }
        }
    }

    private static void start() {
        Injector parentInjector = baseConfiguration();
        migrateDb(parentInjector);
        parentInjector.getInstance(BatchesProcessingExecutor.class).run();
    }

    private static void migrateDb(Injector parentInjector) {
        LOGGER.info("Migrating DB");
        Migration instance = parentInjector.getInstance(Migration.class);
        instance.migrate();
    }

    private static Injector baseConfiguration() {
        return Guice.createInjector(stage(),
                new Cfg4jModule("app.yaml"),
                new JooqModule(),
                new LGONodeModule());
    }

    private static Stage stage() {
        return Configuration.stage("app", LOGGER);
    }


    private static final Logger LOGGER = LoggerFactory.getLogger(LGONode.class);
}
