package lgonode.domain;

import java.util.List;

public interface OrderRepository {
    void save(List<Order> orders);
}
