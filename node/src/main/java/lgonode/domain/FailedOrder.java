package lgonode.domain;

import com.google.common.base.Splitter;

import java.util.Map;

public class FailedOrder {

    public FailedOrder(long id, long batchId, String reason, String details) {
        this.id = id;
        this.batchId = batchId;
        this.reason = reason;
        this.details = Splitter.on(',').withKeyValueSeparator('=').split(details);
    }

    public FailedOrder(long id, long batchId, String reason, Map<String, String> details) {
        this.id = id;
        this.batchId = batchId;
        this.reason = reason;
        this.details = details;
    }

    public final long id;
    public final long batchId;
    public final String reason;
    public Map<String, String> details;
}
