package lgonode.domain;

public class Order {

    public Order(long id, long batchId, String keyId, long creationDate, String type, String direction, String baseCurrency, String quoteCurrency, String quantity, String price) {
        this.id = id;
        this.batchId = batchId;
        this.keyId = keyId;
        this.type = type;
        this.direction = direction;
        this.baseCurrency = baseCurrency;
        this.quoteCurrency = quoteCurrency;
        this.quantity = quantity;
        this.price = price;
        this.creationDate = creationDate;
    }

    public final long id;
    public final long batchId;
    public final String keyId;
    public final String type;
    public final String direction;
    public final String baseCurrency;
    public final String quoteCurrency;
    public final String quantity;
    public final String price;
    public final long creationDate;
}
