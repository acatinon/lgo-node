package lgonode.domain;

public class EncryptedOrder {

    public EncryptedOrder(long id, long batchId, String keyId, byte[] data) {
        this.id = id;
        this.batchId = batchId;
        this.keyId = keyId;
        this.data = data;
    }

    public final long id;
    public final long batchId;
    public final String keyId;
    public final byte[] data;
}
