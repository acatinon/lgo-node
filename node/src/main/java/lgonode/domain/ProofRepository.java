package lgonode.domain;

public interface ProofRepository {
    void save(Proof proof);
}
