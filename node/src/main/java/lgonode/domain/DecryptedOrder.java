package lgonode.domain;

public class DecryptedOrder {

    public DecryptedOrder(long id, long batchId, String keyId, String data) {
        this.id = id;
        this.batchId = batchId;
        this.keyId = keyId;
        this.data = data;
    }

    public final long id;
    public final long batchId;
    public final String keyId;
    public final String data;
}
