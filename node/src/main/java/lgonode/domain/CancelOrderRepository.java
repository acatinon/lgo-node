package lgonode.domain;

import java.util.List;

public interface CancelOrderRepository {
    void save(List<CancelOrder> cancelOrders);
}
