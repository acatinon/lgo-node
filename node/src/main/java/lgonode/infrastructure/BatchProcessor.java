package lgonode.infrastructure;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Set;

public class BatchProcessor {

    @Inject
    public BatchProcessor(Set<BatchProcessingStep> steps) {
        this.steps = steps;
    }

    public BatchProcessor(Collection<BatchProcessingStep> steps) {
        this.steps = steps;
    }

    public ProcessingResult process(BatchProcessingContext context) {
        ProcessingResult result;
        for (BatchProcessingStep step : steps) {
            result = step.process(context);
            if (result == ProcessingResult.FAILURE || result == ProcessingResult.WAIT) {
                return result;
            }
        }
        return ProcessingResult.CONTINUE;
    }

    private final Collection<BatchProcessingStep> steps;
}
