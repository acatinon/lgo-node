package lgonode.infrastructure.decipher;

import lgonode.domain.EncryptedOrder;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Inject;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.stream.Collectors;

public class OrderDecryptionStep implements BatchProcessingStep {

    private final KeyRetriever keyRetriever;

    @Inject
    public OrderDecryptionStep(KeyRetriever keyRetriever) {
        this.keyRetriever = keyRetriever;
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        retrieveKeys(context);
        return context
                .encryptedOrders()
                .stream()
                .map(encryptedOrder -> decipher(encryptedOrder, context))
                .anyMatch(ProcessingResult.FAILURE::equals) ? ProcessingResult.FAILURE : ProcessingResult.CONTINUE;
    }

    private void retrieveKeys(BatchProcessingContext context) {
        keyRetriever.retrievePrivateKeys(context
                .encryptedOrders()
                .stream()
                .map(o -> o.keyId)
                .distinct()
                .collect(Collectors.toList())
        );
    }

    private ProcessingResult decipher(EncryptedOrder encryptedOrder, BatchProcessingContext context) {
        return keyRetriever.getPrivateKey(encryptedOrder.keyId).map(privateKey -> {
            try {
                String data = decipher(encryptedOrder.data, privateKey);
                context.orderDecrypted(encryptedOrder.id, encryptedOrder.keyId, data);
                return ProcessingResult.CONTINUE;
            } catch (Exception e) {
                LOGGER.error("Not possible to decipher", e);
                return ProcessingResult.FAILURE;
            }
        }).orElse(ProcessingResult.CONTINUE);

    }

    private String decipher(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("RSA/None/OAEPPadding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(data));
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDecryptionStep.class);

}
