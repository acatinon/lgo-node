package lgonode.infrastructure.decipher;

import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class EnsurePrivateKeysAreReleasedStep implements BatchProcessingStep {

    private final KeyRetriever keyRetriever;

    @Inject
    public EnsurePrivateKeysAreReleasedStep(KeyRetriever keyRetriever) {
        this.keyRetriever = keyRetriever;
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        List<String> keyIds = context
                .encryptedOrders()
                .stream()
                .map(o -> o.keyId)
                .distinct()
                .collect(Collectors.toList());
        keyRetriever.retrievePrivateKeys(keyIds);
        List<String> publicKeyAvailablesButNoPrivateKeys = keyIds
                .stream()
                .filter(k -> keyRetriever.getPrivateKey(k).isEmpty() && keyRetriever.isPublicKeyAvailable(k))
                .collect(Collectors.toList());
        if (!publicKeyAvailablesButNoPrivateKeys.isEmpty()) {
            return ProcessingResult.WAIT;
        }
        return ProcessingResult.CONTINUE;
    }

}
