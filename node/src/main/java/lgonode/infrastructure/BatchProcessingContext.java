package lgonode.infrastructure;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import lgonode.domain.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class BatchProcessingContext {

    private BatchProcessingContext(String batchDirectory, String lastBatchHash) {
        this.batchDirectory = batchDirectory;
        this.batchId = Long.parseLong(batchDirectory);
        this.lastBatchHash = lastBatchHash;
    }

    public static BatchProcessingContext create(String batchIdAsString, String lastBatchHash) {
        return new BatchProcessingContext(batchIdAsString, lastBatchHash);
    }

    public static BatchProcessingContext create(long batchId, String lastBatchHash) {
        return new BatchProcessingContext(formatBatchId(batchId), lastBatchHash);
    }

    public BatchProcessingContext encryptedOrderRead(long id, String keyId, byte[] data) {
        encryptedOrders.add(new EncryptedOrder(id, batchId, keyId, data));
        return this;
    }

    public BatchProcessingContext failedOrderRead(long id, String reason, Map<String, String> details) {
        failedOrders.add(new FailedOrder(id, batchId, reason, details));
        return this;
    }

    public BatchProcessingContext metadataRead(long id, String hash, String previousHash, int orderCount, boolean cancelOrderBooks) {
        metadata = new Metadata(id, hash, previousHash, orderCount, cancelOrderBooks);
        return this;
    }

    public BatchProcessingContext otsProofRead(String batchHash, byte[] content) {
        otsProof = new Proof(batchId, Hashing.sha256().hashString(batchHash, Charsets.UTF_8).toString(), content);
        return this;
    }

    public BatchProcessingContext orderDecrypted(long id, String keyId, String data) {
        decryptedOrders.add(new DecryptedOrder(id, batchId, keyId, data));
        return this;
    }

    public BatchProcessingContext orderContentRead(long id, String keyId, long creationDate, String type, String direction, String baseCurrency, String quoteCurrency, String quantity, String price) {
        orders.add(new Order(id, batchId, keyId, creationDate, type, direction, baseCurrency, quoteCurrency, quantity, price));
        return this;
    }

    public BatchProcessingContext cancelOrderContentRead(long orderId, String keyId, long creationDate, long orderIdToCancel) {
        cancelOrders.add(new CancelOrder(orderId, batchId, keyId, creationDate, orderIdToCancel));
        return this;
    }

    public void nextBatch() {
        lastBatchHash = metadata.hash;
        batchId++;
        batchDirectory = formatBatchId(batchId);
        metadata = null;
        encryptedOrders.clear();
        failedOrders.clear();
        decryptedOrders.clear();
        orders.clear();
        cancelOrders.clear();
    }

    private static String formatBatchId(long batchId) {
        return String.format("%010d", batchId);
    }

    public String batchDirectory() {
        return batchDirectory;
    }

    public long batchId() {
        return batchId;
    }

    public String lastBatchHash() {
        return lastBatchHash;
    }

    public Metadata metadata() {
        return metadata;
    }

    public List<EncryptedOrder> encryptedOrders() {
        return Collections.unmodifiableList(encryptedOrders);
    }

    public List<FailedOrder> failedOrders() {
        return Collections.unmodifiableList(failedOrders);
    }

    public List<DecryptedOrder> decryptedOrders() {
        return Collections.unmodifiableList(decryptedOrders);
    }

    public List<Order> orders() {
        return Collections.unmodifiableList(orders);
    }

    public List<CancelOrder> cancelOrders() {
        return Collections.unmodifiableList(cancelOrders);
    }

    public Proof otsProof() {
        return otsProof;
    }

    private String batchDirectory;
    private long batchId;
    private Proof otsProof;
    private String lastBatchHash;
    private Metadata metadata;
    private List<EncryptedOrder> encryptedOrders = new ArrayList<>();
    private List<FailedOrder> failedOrders = new ArrayList<>();
    private List<DecryptedOrder> decryptedOrders = new ArrayList<>();
    private List<Order> orders = new ArrayList<>();
    private List<CancelOrder> cancelOrders = new ArrayList<>();
}
