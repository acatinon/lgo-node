package lgonode.infrastructure.file;

import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;
import lgonode.infrastructure.avro.FileNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BatchFilesPresenceVerificationStep implements BatchProcessingStep {

    @Inject
    public BatchFilesPresenceVerificationStep(@Named("storage.batch.path") String batchStoragePath) {
        this.batchStoragePath = Paths.get(batchStoragePath);
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        Path path = FileNames.batchDirectory(batchStoragePath, context.batchDirectory());
        if (!path.toFile().exists()) {
            return ProcessingResult.WAIT;
        }
        try {
            List<String> collect = Files.list(path).map(Path::toFile).map(File::getName).collect(Collectors.toList());
            List<String> expectedFileNames = Arrays.asList(
                    FileNames.proofFileName(context.batchDirectory()),
                    FileNames.ordersFileName(context.batchDirectory()),
                    FileNames.metadataFileName(context.batchDirectory()),
                    FileNames.failedOrdersFileName(context.batchDirectory())
            );
            if (!collect.containsAll(expectedFileNames)) {
                return ProcessingResult.WAIT;
            }
        } catch (IOException e) {
            LOGGER.error("Cannot list batch files", e);
            return ProcessingResult.FAILURE;
        }
        return ProcessingResult.CONTINUE;
    }

    private final Path batchStoragePath;
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchFilesPresenceVerificationStep.class);
}
