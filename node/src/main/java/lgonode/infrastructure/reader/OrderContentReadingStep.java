package lgonode.infrastructure.reader;

import lgonode.domain.DecryptedOrder;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrderContentReadingStep implements BatchProcessingStep {

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        for (DecryptedOrder order : context.decryptedOrders()) {
            try {
                readData(order, context);
            } catch (Exception e) {
                LOGGER.error("Not possible to read order content: "+order.data, e);
                return ProcessingResult.FAILURE;
            }
        }
        return ProcessingResult.CONTINUE;
    }

    private void readData(DecryptedOrder order, BatchProcessingContext context) {
        if (order.data == null) {
            LOGGER.warn("Can't read order data for order: " + order.id);
            return;
        }
        String[] rawOrder = parseRawOrder(order.data);
        if (isOrder(rawOrder)) {
            parseOrder(order.id, order.keyId, rawOrder, context);
            return;
        }
        parseCancelOrder(order.id, order.keyId, rawOrder, context);
    }

    private String[] parseRawOrder(String order) {
        return order.split(",");
    }

    private boolean isOrder(String[] data) {
        return !isCancelOrder(data);
    }

    private boolean isCancelOrder(String[] orderType) {
        return "C".equals(orderType[0]);
    }

    private void parseOrder(long id, String keyId, String[] data, BatchProcessingContext context) {
        String orderType = data[0];
        String direction = data[1];
        String[] pair = data[2].split("-");
        String quantity = data[3];
        String price = data[4];
        long timestamp = Long.parseLong(data[6]);
        context.orderContentRead(
                id,
                keyId, timestamp*1000,
                orderType,
                direction,
                pair[0],
                pair[1],
                quantity,
                price
        );
    }

    private void parseCancelOrder(long orderId, String keyId, String[] data, BatchProcessingContext context) {
        long creationDate = Long.parseLong(data[2]) *1000;
        context.cancelOrderContentRead(orderId, keyId, creationDate, Long.parseLong(data[1]));
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderContentReadingStep.class);

}