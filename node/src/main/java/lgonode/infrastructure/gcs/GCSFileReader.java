package lgonode.infrastructure.gcs;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.zip.GZIPInputStream;

public abstract class GCSFileReader {

    static Storage storage = StorageOptions.getUnauthenticatedInstance().getService();

    Path copyFileToTemp(Blob value) throws IOException {
        Path tempFile = Files.createTempFile(value.getName(), null);
        copyFile(value, tempFile);
        return tempFile;
    }

    private void copyFile(Blob value, Path destination) throws IOException {
        if (!Files.exists(destination)) {
            Files.createFile(destination);
        }
        value.downloadTo(destination);
        if ("gzip".equals(value.getContentEncoding())) {
            byte[] bytes = Files.readAllBytes(destination);
            byte[] decompress = decompress(bytes);
            Files.write(destination, decompress, StandardOpenOption.TRUNCATE_EXISTING);
        }
    }

    public static byte[] decompress(final byte[] compressed) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(compressed);
        GZIPInputStream gis = new GZIPInputStream(bis);
        return IOUtils.toByteArray(gis);
    }
}
