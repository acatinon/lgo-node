package lgonode.infrastructure.gcs;

import com.google.cloud.storage.Blob;
import lgonode.configuration.GCSConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Path;
import java.util.Optional;

public class GCSKeyBucketReader extends GCSFileReader {

    @Inject
    public GCSKeyBucketReader(GCSConfiguration gcsConfiguration) {
        this.gcsConfiguration = gcsConfiguration;
    }

    public Optional<Reader> loadPrivateKey(String keyId) {
        return loadKey(keyId, "_private");
    }

    public boolean isPublicKeyAvailable(String keyId) {
        Blob blob = getBlob(keyId, "_public");
        return !(blob == null);
    }

    private Optional<Reader> loadKey(String keyId, String suffix) {
        Blob blob = getBlob(keyId, suffix);
        if (blob == null) {
            return Optional.empty();
        }
        try {
            Path path = copyFileToTemp(blob);
            return Optional.of(new FileReader(path.toFile()));
        } catch (IOException e) {
            LOGGER.error("Keys not retrievable: " + keyId, e);
            return Optional.empty();
        }
    }

    private Blob getBlob(String keyId, String suffix) {
        return storage.get(gcsConfiguration.key(), keyId + suffix +
                ".pem");
    }

    private final GCSConfiguration gcsConfiguration;
    private static final Logger LOGGER = LoggerFactory.getLogger(GCSKeyBucketReader.class);
}
