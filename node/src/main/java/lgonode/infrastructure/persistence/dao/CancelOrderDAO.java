package lgonode.infrastructure.persistence.dao;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.impl.DSL;

public class CancelOrderDAO {
    public static Table<Record> TABLE = DSL.table("cancel_order_view");
    public static Field<Long> ID = DSL.field("id", Long.class);
    public static Field<Long> BATCH_ID = DSL.field("batch_id", Long.class);
    public static Field<String> KEY_ID = DSL.field("key_id", String.class);
    public static Field<Long> CREATION_DATE = DSL.field("creation_date", Long.class);
    public static Field<Long> ORDER_TO_CANCEL = DSL.field("order_to_cancel", Long.class);
}