package lgonode.infrastructure.persistence;

import lgonode.domain.*;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public class BatchPersistenceStep implements BatchProcessingStep {

    @Inject
    public BatchPersistenceStep(BatchRepository batchRepository,
                                OrderRepository orderRepository,
                                FailedOrderRepository failedOrderRepository,
                                CancelOrderRepository cancelOrderRepository,
                                ProofRepository proofRepository) {
        this.batchRepository = batchRepository;
        this.orderRepository = orderRepository;
        this.failedOrderRepository = failedOrderRepository;
        this.cancelOrderRepository = cancelOrderRepository;
        this.proofRepository = proofRepository;
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        try {
            batchRepository.save(context.metadata());
            orderRepository.save(context.orders());
            failedOrderRepository.save(context.failedOrders());
            cancelOrderRepository.save(context.cancelOrders());
            proofRepository.save(context.otsProof());
        } catch (Exception e) {
            LOGGER.error("Persistence failure", e);
            return ProcessingResult.FAILURE;
        }
        return ProcessingResult.CONTINUE;
    }

    private final BatchRepository batchRepository;
    private final OrderRepository orderRepository;
    private final FailedOrderRepository failedOrderRepository;
    private final CancelOrderRepository cancelOrderRepository;
    private final ProofRepository proofRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchPersistenceStep.class);
}
