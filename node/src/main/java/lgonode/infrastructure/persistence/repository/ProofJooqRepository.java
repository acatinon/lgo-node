package lgonode.infrastructure.persistence.repository;

import lgonode.domain.Proof;
import lgonode.domain.ProofRepository;
import org.jooq.DSLContext;

import javax.inject.Inject;

import static lgonode.infrastructure.persistence.dao.ProofDAO.*;

public class ProofJooqRepository implements ProofRepository {

    @Inject
    public ProofJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void save(Proof proof) {
        this.dsl.insertInto(TABLE)
                .columns(BATCH_ID, TIMESTAMPED_HASH, CONTENT)
                .values(proof.batchId, proof.timestampedHash, proof.content)
                .execute();
    }

    private final DSLContext dsl;
}
