package lgonode.infrastructure.persistence.repository;

import com.google.common.base.Joiner;
import lgonode.domain.FailedOrder;
import lgonode.domain.FailedOrderRepository;
import org.jooq.DSLContext;

import javax.inject.Inject;
import java.util.List;

import static lgonode.infrastructure.persistence.dao.FailedOrderDAO.*;

public class FailedOrderJooqRepository implements FailedOrderRepository {

    @Inject
    public FailedOrderJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void save(List<FailedOrder> failedOrderList) {
        var insert = this.dsl.insertInto(TABLE).columns(ID, BATCH_ID, REASON, DETAILS);
        for (FailedOrder failedOrder : failedOrderList) {
            insert.values(failedOrder.id, failedOrder.batchId, failedOrder.reason, Joiner.on(",").withKeyValueSeparator("=").join(failedOrder.details));
        }
        insert.execute();
    }

    private final DSLContext dsl;
}
