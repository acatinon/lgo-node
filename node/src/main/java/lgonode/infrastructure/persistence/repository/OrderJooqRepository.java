package lgonode.infrastructure.persistence.repository;

import lgonode.domain.Order;
import lgonode.domain.OrderRepository;
import org.jooq.DSLContext;

import javax.inject.Inject;
import java.util.List;

import static lgonode.infrastructure.persistence.dao.OrderDAO.*;

public class OrderJooqRepository implements OrderRepository {

    @Inject
    public OrderJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void save(List<Order> orderList) {
        var insert = this.dsl.insertInto(TABLE).columns(ID, BATCH_ID, KEY_ID, CREATION_DATE, TYPE, DIRECTION, BASE_CURRENCY, QUOTE_CURRENCY, QUANTITY, PRICE);
        for (Order order : orderList) {
            insert = insert.values(order.id, order.batchId, order.keyId, order.creationDate, order.type, order.direction, order.baseCurrency, order.quoteCurrency, order.quantity, order.price);
        }
        insert.execute();
    }

    private final DSLContext dsl;
}
