package lgonode.infrastructure.persistence.repository;

import lgonode.domain.BatchRepository;
import lgonode.domain.Metadata;
import org.jooq.DSLContext;

import javax.inject.Inject;

import static lgonode.infrastructure.persistence.dao.BatchDAO.*;

public class BatchJooqRepository implements BatchRepository {

    @Inject
    public BatchJooqRepository(DSLContext dsl) {
        this.dsl = dsl;
    }

    @Override
    public void save(Metadata metadata) {
        this.dsl.insertInto(TABLE)
                .columns(ID, HASH, PREVIOUS_HASH, ORDER_COUNT, CANCEL_ORDER_BOOKS)
                .values(metadata.id, metadata.hash, metadata.previousHash, metadata.orderCount, metadata.cancelOrderBooks)
                .execute();
    }

    @Override
    public Metadata lastBatch() {
        return this.dsl.select(ID, HASH, PREVIOUS_HASH, ORDER_COUNT, CANCEL_ORDER_BOOKS)
                .from(TABLE)
                .orderBy(ID.desc())
                .limit(1)
                .fetchOneInto(Metadata.class);
    }

    private final DSLContext dsl;
}
