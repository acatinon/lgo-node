package lgonode.infrastructure.avro;

import com.google.common.collect.Maps;
import lgonode.domain.Metadata;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.ProcessingResult;
import group.lgo.FailedOrder;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BatchFilesReadingStep implements BatchProcessingStep {

    @Inject
    public BatchFilesReadingStep(@Named("storage.batch.path") String batchStoragePath) {
        this.batchStoragePath = Paths.get(batchStoragePath);
    }

    @Override
    public ProcessingResult process(BatchProcessingContext context) {
        try {
            Metadata metadata = readMetadata(context);
            readFailedOrders(context);
            readOrders(context);
            readUpgradedOtsProof(context, metadata);
        } catch (IOException e) {
            LOGGER.error("IO error while processing batch files", e);
            return ProcessingResult.FAILURE;
        }
        return ProcessingResult.CONTINUE;
    }

    Metadata readMetadata(BatchProcessingContext context) throws IOException {
        String metadataContent = new String(Files.readAllBytes(FileNames.metadataFileFor(batchStoragePath, context.batchDirectory())));
        Matcher matcher = metadataPattern.matcher(metadataContent);
        if (!matcher.matches() || matcher.groupCount() != 5) {
            throw new IOException("Cannot read metadata content");
        }
        long id = Long.parseLong(matcher.group(1));
        String hash = matcher.group(2);
        String previousHash = matcher.group(3);
        int orderCount = Integer.parseInt(matcher.group(4));
        boolean cancelOrderBooks = matcher.group(5) != null;
        context.metadataRead(id, hash, previousHash, orderCount, cancelOrderBooks);
        return context.metadata();
    }

    void readFailedOrders(BatchProcessingContext context) throws IOException {
        File failedOrdersFile = FileNames.failedOrdersFor(batchStoragePath, context.batchDirectory()).toFile();
        try (var failedOrders = new DataFileReader<>(failedOrdersFile, new SpecificDatumReader<>(FailedOrder.class))) {
            for (var failedOrder : failedOrders) {
                context.failedOrderRead(failedOrder.getId(), failedOrder.getFailureReason().toString(), getDetails(failedOrder));
            }
        }
    }

    private Map<String, String> getDetails(FailedOrder failedOrder) {
        Map<String, String> result = Maps.newHashMap();
        if (failedOrder.getDetails() == null) {
            return result;
        }
        for (Map.Entry<CharSequence, CharSequence> entry : failedOrder.getDetails().entrySet()) {
            result.put(entry.getKey().toString(), entry.getValue().toString());
        }
        return result;
    }

    void readOrders(BatchProcessingContext context) throws IOException {
        File ordersFile = FileNames.ordersFor(batchStoragePath, context.batchDirectory()).toFile();
        try (var orders = new DataFileReader<>(ordersFile, new SpecificDatumReader<>(group.lgo.Order.class))) {
            for (group.lgo.Order order : orders) {
                context.encryptedOrderRead(order.getId(), order.getKeyId().toString(), data(order));
            }
        }
    }

    void readUpgradedOtsProof(BatchProcessingContext context, Metadata metadata) throws IOException {
        Path proof = FileNames.upgradedProofFileFor(batchStoragePath, context.batchDirectory());
        context.otsProofRead(metadata.hash, Files.readAllBytes(proof));
    }

    private byte[] data(group.lgo.Order order) {
        byte[] data = new byte[order.getData().capacity()];
        order.getData().get(data);
        return data;
    }

    private final Path batchStoragePath;
    private static Pattern metadataPattern = Pattern.compile("ID=([0-9]*)\nHASH=(.*)\nPREVIOUS_HASH=(.*)\nORDER_COUNT=([0-9]*)(\nCANCEL_ORDER_BOOKS=true)?");
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchFilesReadingStep.class);
}
