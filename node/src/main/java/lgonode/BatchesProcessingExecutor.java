package lgonode;

import lgonode.domain.BatchRepository;
import lgonode.domain.Metadata;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.BatchProcessor;
import lgonode.infrastructure.ProcessingResult;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class BatchesProcessingExecutor {

    @Inject
    public BatchesProcessingExecutor(BatchProcessor batchProcessor, BatchRepository batchRepository, DSLContext context) {
        this.batchProcessor = batchProcessor;
        this.batchRepository = batchRepository;
        this.context = context;
        this.shutdownRequested = false;
    }

    public void run() {
        Runtime.getRuntime().addShutdownHook(new ShutdownHandler());
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledFuture = scheduledExecutorService.scheduleWithFixedDelay(
                this::processBatches,
                0,
                5,
                TimeUnit.SECONDS);
    }

    private void processBatches() {
        try {
            AtomicReference<ProcessingResult> processingResult = new AtomicReference<>();
            do {
                initiateContext();
                try {
                    context.transaction(() -> processingResult.set(processBatch()));
                } catch (Exception e) {
                    LOGGER.error("Can't process batch");
                }
            } while (ProcessingResult.CONTINUE == processingResult.get() && !shutdownRequested);
        } catch (Exception e ) {
            LOGGER.error("ERROR", e);
        }
    }

    private ProcessingResult processBatch() {
        LOGGER.info("Starting processing batch " + batchProcessingContext.batchId());
        ProcessingResult processingResult = batchProcessor.process(batchProcessingContext);
        LOGGER.info("End processing batch " + batchProcessingContext.batchId() + ", result : " + processingResult.name());
        return processingResult;
    }

    private void initiateContext() {
        if (batchProcessingContext == null || batchProcessingContext.metadata() == null) {
            Metadata lastBatch = batchRepository.lastBatch();
            if (lastBatch == null) {
                batchProcessingContext = BatchProcessingContext.create(0, null);
            } else {
                batchProcessingContext = BatchProcessingContext.create(lastBatch.id + 1, lastBatch.hash);
            }
        } else {
            batchProcessingContext.nextBatch();
        }
    }

    private class ShutdownHandler extends Thread {
        @Override
        public void run() {
            LOGGER.info("Shutdown requested");
            BatchesProcessingExecutor.this.shutdownRequested = true;
            scheduledExecutorService.shutdown();
        }
    }

    private final BatchProcessor batchProcessor;
    private final BatchRepository batchRepository;
    private DSLContext context;
    private boolean shutdownRequested;
    private ScheduledFuture<?> scheduledFuture;
    private ScheduledExecutorService scheduledExecutorService;
    private BatchProcessingContext batchProcessingContext = null;
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchProcessor.class);
}
