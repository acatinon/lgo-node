package lgonode.configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;
import lgonode.domain.*;
import lgonode.infrastructure.BatchProcessingStep;
import lgonode.infrastructure.BatchProcessor;
import lgonode.infrastructure.avro.BatchFilesReadingStep;
import lgonode.infrastructure.chain.BatchChainingValidationStep;
import lgonode.infrastructure.chain.BatchHashValidationStep;
import lgonode.infrastructure.decipher.EnsurePrivateKeysAreReleasedStep;
import lgonode.infrastructure.decipher.OrderDecryptionStep;
import lgonode.infrastructure.decipher.KeyRetriever;
import lgonode.infrastructure.file.BatchFilesPresenceVerificationStep;
import lgonode.infrastructure.gcs.GCSKeyBucketReader;
import lgonode.infrastructure.persistence.BatchPersistenceStep;
import lgonode.infrastructure.persistence.repository.*;
import lgonode.infrastructure.reader.OrderContentReadingStep;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.cfg4j.provider.ConfigurationProvider;

import javax.inject.Named;
import java.security.Security;

public class LGONodeModule extends AbstractModule {

    @Override
    public void configure() {
        bind(BatchRepository.class).to(BatchJooqRepository.class).in(Singleton.class);
        bind(CancelOrderRepository.class).to(CancelOrderJooqRepository.class).in(Singleton.class);
        bind(FailedOrderRepository.class).to(FailedOrderJooqRepository.class).in(Singleton.class);
        bind(OrderRepository.class).to(OrderJooqRepository.class).in(Singleton.class);
        bind(ProofRepository.class).to(ProofJooqRepository.class).in(Singleton.class);
        bind(GCSKeyBucketReader.class).in(Singleton.class);
        bind(KeyRetriever.class).in(Singleton.class);
        configureBatchProcessor();
        Security.addProvider(new BouncyCastleProvider());
    }

    private void configureBatchProcessor() {
        bind(OrderContentReadingStep.class).in(Singleton.class);
        Multibinder<BatchProcessingStep> multibinder = Multibinder.newSetBinder(binder(), BatchProcessingStep.class);
        multibinder.addBinding().to(BatchFilesPresenceVerificationStep.class);
        multibinder.addBinding().to(BatchFilesReadingStep.class);
        multibinder.addBinding().to(BatchHashValidationStep.class);
        multibinder.addBinding().to(BatchChainingValidationStep.class);
        multibinder.addBinding().to(EnsurePrivateKeysAreReleasedStep.class);
        multibinder.addBinding().to(OrderDecryptionStep.class);
        multibinder.addBinding().to(OrderContentReadingStep.class);
        multibinder.addBinding().to(BatchPersistenceStep.class);
        bind(BatchProcessor.class).in(Singleton.class);
    }


    @Provides
    @Singleton
    @Named("storage.batch.path")
    public String batchStoragePath(ConfigurationProvider provider) {
        return provider.getProperty("storage.batch.path", String.class);
    }


    @Provides
    @Singleton
    @Named("storage.key.path")
    public String keyStoragePath(ConfigurationProvider provider) {
        return provider.getProperty("storage.key.path", String.class);
    }

    @Provides
    @Singleton
    public GCSConfiguration gcsConfiguration(ConfigurationProvider configurationProvider) {
        return configurationProvider.bind("gcs.bucket", GCSConfiguration.class);
    }
}
