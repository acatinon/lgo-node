package lgonode.configuration;

public interface GCSConfiguration {

    String batch();

    String key();

}
