package lgonode.configuration;

import com.google.common.base.Strings;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.zaxxer.hikari.HikariDataSource;
import org.cfg4j.provider.ConfigurationProvider;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.ThreadLocalTransactionProvider;

public class JooqModule extends AbstractModule {
    @Override
    protected void configure() {

    }

    @Provides
    @Singleton
    public Migration flyway(HikariDataSource dataSource) {
        return new Migration(dataSource);
    }

    @Provides
    @Singleton
    public HikariDataSource dataSource(PostgreSqlConfiguration configuration) {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setJdbcUrl(configuration.url());
        hikariDataSource.setUsername(configuration.user());
        hikariDataSource.setAutoCommit(false);
        if (!Strings.isNullOrEmpty(configuration.password())) {
            hikariDataSource.setPassword(configuration.password());
        }
        return hikariDataSource;
    }

    @Provides
    @Singleton
    public DSLContext dslContext(HikariDataSource dataSource) {
        Configuration configuration = new DefaultConfiguration()
                .set(new ThreadLocalTransactionProvider(new DataSourceConnectionProvider(dataSource)))
                .set(SQLDialect.POSTGRES);
        return DSL.using(configuration);
    }

    @Provides
    @Singleton
    public PostgreSqlConfiguration configuration(ConfigurationProvider configurationProvider) {
        return configurationProvider.bind("postgres", PostgreSqlConfiguration.class);
    }
}
