package lgonode.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;

public class Migration {

    public Migration(HikariDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void migrate() {
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setLocations("lgonode.db.migration");
        flyway.migrate();
    }
    private final HikariDataSource dataSource;
}
