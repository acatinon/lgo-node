package lgonode.configuration;

public interface PostgreSqlConfiguration {

    String url();

    String user();

    String password();
}
