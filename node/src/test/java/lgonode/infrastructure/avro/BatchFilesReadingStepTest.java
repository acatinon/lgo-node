package lgonode.infrastructure.avro;

import lgonode.domain.EncryptedOrder;
import lgonode.domain.FailedOrder;
import lgonode.domain.Metadata;
import lgonode.domain.Proof;
import lgonode.infrastructure.BatchProcessingContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class BatchFilesReadingStepTest {

    @BeforeEach
    void setUp() {
        Path path = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        batchFilesReader = new BatchFilesReadingStep(path.toString());
        context = BatchProcessingContext.create("0000000000", "lastBatchHash");
    }

    @Test
    void reads_orders() throws IOException {
        batchFilesReader.readOrders(context);

        List<EncryptedOrder> orders = context.encryptedOrders();
        assertThat(orders).hasSize(1);
        assertThat(orders.get(0).id).isEqualTo(155075557546300001L);
        assertThat(orders.get(0).batchId).isEqualTo(0);
        assertThat(orders.get(0).keyId).isEqualTo("465beb3d-5b67-4051-90e2-b0a54caa6de1");
    }

    @Test
    void reads_failed_orders() throws IOException {
        batchFilesReader.readFailedOrders(context);

        List<FailedOrder> failedOrders = context.failedOrders();
        assertThat(failedOrders).hasSize(1);
        assertThat(failedOrders.get(0).id).isEqualTo(155075557546300001L);
        assertThat(failedOrders.get(0).batchId).isEqualTo(0);
        assertThat(failedOrders.get(0).reason).isEqualTo("invalid-key");
        assertThat(failedOrders.get(0).details).isEmpty();
    }

    @Test
    void reads_metadata() throws IOException {
        batchFilesReader.readMetadata(context);

        Metadata metadata = context.metadata();
        assertThat(metadata.id).isEqualTo(0);
        assertThat(metadata.hash).isEqualTo("e59ad0e36fd3896badd9a7534ec3c9889220a1cb6f0014f4a72f2e05ad897b11");
        assertThat(metadata.previousHash).isEmpty();
        assertThat(metadata.orderCount).isEqualTo(1);
        assertThat(metadata.cancelOrderBooks).isFalse();
    }

    @Test
    void reads_metadata_with_cancel_order_book() throws IOException {
        Path path = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        BatchFilesReadingStep batchFilesReader = new BatchFilesReadingStep(path.toString());
        BatchProcessingContext context = BatchProcessingContext.create("0002840150", "lastBatchHash");

        batchFilesReader.readMetadata(context);

        Metadata metadata = context.metadata();
        assertThat(metadata.id).isEqualTo(2840150);
        assertThat(metadata.cancelOrderBooks).isTrue();
    }

    @Test
    void reads_ots_upgraded_proof() throws IOException {
        batchFilesReader.readUpgradedOtsProof(context, new Metadata(1L, "hash", "prev", 1, false));

        Proof proof = context.otsProof();
        assertThat(proof).isNotNull();
        assertThat(proof.batchId).isEqualTo(0);
        assertThat(proof.timestampedHash).isEqualTo("d04b98f48e8f8bcc15c6ae5ac050801cd6dcfd428fb5f9e65c4e16e7807340fa");
        assertThat(proof.content).isNotEmpty();
    }

    private BatchFilesReadingStep batchFilesReader;
    private BatchProcessingContext context;
}
