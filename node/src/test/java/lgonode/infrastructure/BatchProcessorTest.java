package lgonode.infrastructure;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class BatchProcessorTest {

    @Test
    void runs_all_steps_with_same_context() {
        BatchProcessingStep step1 = mock(BatchProcessingStep.class);
        BatchProcessingStep step2 = mock(BatchProcessingStep.class);
        when(step1.process(any())).thenReturn(ProcessingResult.CONTINUE);
        when(step2.process(any())).thenReturn(ProcessingResult.CONTINUE);
        BatchProcessor batchProcessor = new BatchProcessor(Set.of(step1, step2));

        ProcessingResult processingResult = batchProcessor.process(BatchProcessingContext.create("001", "plop"));

        assertThat(processingResult).isEqualTo(ProcessingResult.CONTINUE);
        var captor = ArgumentCaptor.forClass(BatchProcessingContext.class);
        verify(step1).process(captor.capture());
        assertThat(captor.getValue().batchDirectory()).isEqualTo("001");
        assertThat(captor.getValue().batchId()).isEqualTo(1);
        var captor2 = ArgumentCaptor.forClass(BatchProcessingContext.class);
        verify(step2).process(captor2.capture());
        assertThat(captor2.getValue().batchDirectory()).isEqualTo("001");
        assertThat(captor2.getValue().batchId()).isEqualTo(1);
        assertThat(captor.getValue()).isSameAs(captor2.getValue());
    }

    @Test
    void stops_at_the_first_failure() {
        BatchProcessingStep step1 = mock(BatchProcessingStep.class);
        BatchProcessingStep step2 = mock(BatchProcessingStep.class);
        when(step1.process(any())).thenReturn(ProcessingResult.FAILURE);
        BatchProcessor batchProcessor = new BatchProcessor(Arrays.asList(step1, step2));

        ProcessingResult processingResult = batchProcessor.process(BatchProcessingContext.create("001", "plop"));

        assertThat(processingResult).isEqualTo(ProcessingResult.FAILURE);
        var captor = ArgumentCaptor.forClass(BatchProcessingContext.class);
        verify(step1).process(captor.capture());
        assertThat(captor.getValue().batchDirectory()).isEqualTo("001");
        assertThat(captor.getValue().batchId()).isEqualTo(1);
        verifyZeroInteractions(step2);
    }

    @Test
    void stops_at_the_first_wait() {
        BatchProcessingStep step1 = mock(BatchProcessingStep.class);
        BatchProcessingStep step2 = mock(BatchProcessingStep.class);
        when(step1.process(any())).thenReturn(ProcessingResult.WAIT);
        BatchProcessor batchProcessor = new BatchProcessor(Arrays.asList(step1, step2));

        ProcessingResult processingResult = batchProcessor.process(BatchProcessingContext.create("001", "plop"));

        assertThat(processingResult).isEqualTo(ProcessingResult.WAIT);
        var captor = ArgumentCaptor.forClass(BatchProcessingContext.class);
        verify(step1).process(captor.capture());
        assertThat(captor.getValue().batchDirectory()).isEqualTo("001");
        assertThat(captor.getValue().batchId()).isEqualTo(1);
        verifyZeroInteractions(step2);
    }
}