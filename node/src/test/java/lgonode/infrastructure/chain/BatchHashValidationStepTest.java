package lgonode.infrastructure.chain;

import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.ProcessingResult;
import lgonode.infrastructure.avro.BatchFilesReadingStep;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

class BatchHashValidationStepTest {

    @Test
    void validates_first_batch_hash() {
        Path pathBatch = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        context = BatchProcessingContext.create("0000000000", null);
        new BatchFilesReadingStep(pathBatch.toString()).process(context);

        ProcessingResult result = new BatchHashValidationStep().process(context);

        assertThat(result).isEqualTo(ProcessingResult.CONTINUE);
    }

    @Test
    void validates_hash() {
        Path pathBatch = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        context = BatchProcessingContext.create("0002020005", "fad90a7c9348c63fd1d3d2d435de4a377a73bb80154ee4ddb705b50bce901550");
        new BatchFilesReadingStep(pathBatch.toString()).process(context);

        ProcessingResult result = new BatchHashValidationStep().process(context);

        assertThat(result).isEqualTo(ProcessingResult.CONTINUE);
    }

    @Test
    void does_not_validate_bad_hash() {
        Path pathBatch = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        context = BatchProcessingContext.create("0002020005", "fad90a7c9348c63fd1d3d2d435de4a377a73bb80154ee4ddb705b50bce901550");
        new BatchFilesReadingStep(pathBatch.toString()).process(context);
        context.metadataRead(context.batchId(), "plop", "", 22, false);

        ProcessingResult result = new BatchHashValidationStep().process(context);

        assertThat(result).isEqualTo(ProcessingResult.FAILURE);
    }

    @Test
    void does_validate_batch_hash_with_no_order() {
        Path pathBatch = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        context = BatchProcessingContext.create("0002840150", "80ebb0d3f9b0dd582373a4b0761cd50515bd6b48204897afe00d86f0538ed82c");
        new BatchFilesReadingStep(pathBatch.toString()).process(context);

        ProcessingResult result = new BatchHashValidationStep().process(context);

        assertThat(result).isEqualTo(ProcessingResult.CONTINUE);
    }

    private BatchProcessingContext context;
}