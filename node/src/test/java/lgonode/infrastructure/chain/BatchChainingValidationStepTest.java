package lgonode.infrastructure.chain;

import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.ProcessingResult;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BatchChainingValidationStepTest {

    @Test
    void fails_when_hash_does_not_match() {
        BatchProcessingContext context = BatchProcessingContext.create(1, "plop");
        context.metadataRead(1, "hello", "failure", 22, false);

        ProcessingResult result = new BatchChainingValidationStep().process(context);

        assertThat(result).isEqualTo(ProcessingResult.FAILURE);
    }

    @Test
    void succeeds_when_hash_does_match() {
        BatchProcessingContext context = BatchProcessingContext.create(1, "plop");
        context.metadataRead(1, "hello", "plop", 22, false);

        ProcessingResult result = new BatchChainingValidationStep().process(context);

        assertThat(result).isEqualTo(ProcessingResult.CONTINUE);
    }

    @Test
    void succeeds_for_first_batch() {
        BatchProcessingContext context = BatchProcessingContext.create(0, null);
        context.metadataRead(0, "hello", null, 22, false);

        ProcessingResult result = new BatchChainingValidationStep().process(context);

        assertThat(result).isEqualTo(ProcessingResult.CONTINUE);
    }
}