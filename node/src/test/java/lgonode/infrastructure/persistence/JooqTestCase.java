package lgonode.infrastructure.persistence;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.RegisterExtension;

import static org.jooq.impl.DSL.table;

public abstract class JooqTestCase {

    @RegisterExtension
    public WithPostgres postgres = WithPostgres.onSchemaAndMigration("test", "lgonode");

    public JooqTestCase(String table) {
        this.account = table(table);
    }

    @AfterEach
    public void tearDown() {
        postgres.truncate(getTable());
    }

    private Table<Record> getTable() {
        return account;
    }

    protected DSLContext dsl() {
        return postgres.dsl();
    }

    private final Table<Record> account;
}
