package lgonode.infrastructure.persistence;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.util.Optional;

public class WithPostgres implements BeforeEachCallback, AfterEachCallback {

    public static WithPostgres onSchemaAndMigration(String schema, String migration) {
        return new WithPostgres(schema, migration);
    }

    private WithPostgres(String schema, String migration) {
        this.schema = schema;
        this.migration = migration;
    }

    @Override
    public void beforeEach(ExtensionContext context) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("lgo");
        hikariConfig.setPassword("lgo");
        hikariConfig.setJdbcUrl(jdbcUrl());
        dataSource = new HikariDataSource(hikariConfig);
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        flyway.setSchemas("test");
        flyway.setLocations(migration + "/db/migration");
        flyway.migrate();
        dsl = DSL.using(dataSource, SQLDialect.POSTGRES);
    }

    private String jdbcUrl() {
        return Optional.ofNullable(System.getenv("POSTGRES_URL")).orElse("jdbc:postgresql://localhost:5437/lgonode?")
                + "&currentSchema=" + schema;
    }

    @Override
    public void afterEach(ExtensionContext context) {
        dsl.dropSchema(schema).cascade().execute();
        dsl.close();
        dataSource.close();
    }

    public DSLContext dsl() {
        return dsl;
    }

    public void truncate(Table<Record> table) {
        dsl.truncate(table).cascade().execute();
    }

    private HikariDataSource dataSource;
    private DSLContext dsl;
    private String schema;
    private String migration;
}
