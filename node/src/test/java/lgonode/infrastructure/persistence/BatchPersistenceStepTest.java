package lgonode.infrastructure.persistence;

import com.google.common.collect.Maps;
import lgonode.domain.*;
import lgonode.infrastructure.BatchProcessingContext;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;

class BatchPersistenceStepTest {

    @Test
    void saves_all_data_starting_with_batch_metadata() {
        BatchRepository batchRepository = mock(BatchRepository.class);
        OrderRepository orderRepository = mock(OrderRepository.class);
        FailedOrderRepository failedOrderRepository = mock(FailedOrderRepository.class);
        CancelOrderRepository cancelOrderRepository = mock(CancelOrderRepository.class);
        ProofRepository proofRepository = mock(ProofRepository.class);
        BatchPersistenceStep step = new BatchPersistenceStep(batchRepository, orderRepository, failedOrderRepository, cancelOrderRepository, proofRepository);
        BatchProcessingContext context = BatchProcessingContext.create("00001", "lastBatchHash");
        context.failedOrderRead(1, "reason", Maps.newHashMap());
        context.orderContentRead(2, "keyid", 123456L, "M", "S", "BTC", "USD", "1.20000000", null);
        context.cancelOrderContentRead(3, "keyid", 123457L, 2);
        context.metadataRead(1, "hash", "previous", 3, false);
        context.otsProofRead("hash", "toto".getBytes());

        step.process(context);

        InOrder inOrder = Mockito.inOrder(batchRepository, orderRepository, failedOrderRepository, cancelOrderRepository, proofRepository);
        inOrder.verify(batchRepository).save(context.metadata());
        inOrder.verify(orderRepository).save(context.orders());
        inOrder.verify(failedOrderRepository).save(context.failedOrders());
        inOrder.verify(cancelOrderRepository).save(context.cancelOrders());
        inOrder.verify(proofRepository).save(context.otsProof());
    }
}