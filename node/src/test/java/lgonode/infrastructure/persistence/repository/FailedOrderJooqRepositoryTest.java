package lgonode.infrastructure.persistence.repository;

import com.google.common.collect.Maps;
import lgonode.domain.FailedOrder;
import lgonode.domain.Metadata;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.FailedOrderDAO;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.Results;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

class FailedOrderJooqRepositoryTest extends JooqTestCase {

    public FailedOrderJooqRepositoryTest() {
        super(FailedOrderDAO.TABLE.getName());
    }

    @BeforeEach
    void setUp() {
        new BatchJooqRepository(dsl()).save(new Metadata(1, "hash", "previousHash", 23, false));
    }

    @Test
    void saves_a_failed_order() {
        HashMap<String, String> data = Maps.newHashMap();
        data.put("counter", "123");
        FailedOrder failedOrder = new FailedOrder(123L, 1, "Bad signature", data);

        new FailedOrderJooqRepository(dsl()).save(singletonList(failedOrder));

        Results failedOrders = dsl().selectFrom(FailedOrderDAO.TABLE).fetchMany();
        assertThat(failedOrders).hasSize(1);
        Result<Record> record = failedOrders.get(0);
        Long id = record.getValue(0, FailedOrderDAO.ID);
        Long batchId = record.getValue(0, FailedOrderDAO.BATCH_ID);
        String reason = record.getValue(0, FailedOrderDAO.REASON);
        String details = record.getValue(0, FailedOrderDAO.DETAILS);
        assertThat(id).isEqualTo(123L);
        assertThat(batchId).isEqualTo(1L);
        assertThat(reason).isEqualTo("Bad signature");
        assertThat(details).isEqualTo("counter=123");
    }
}