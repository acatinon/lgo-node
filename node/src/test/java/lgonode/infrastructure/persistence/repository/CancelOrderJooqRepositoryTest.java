package lgonode.infrastructure.persistence.repository;

import lgonode.domain.CancelOrder;
import lgonode.domain.Metadata;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.CancelOrderDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static lgonode.infrastructure.persistence.dao.CancelOrderDAO.*;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

class CancelOrderJooqRepositoryTest extends JooqTestCase {

    public CancelOrderJooqRepositoryTest() {
        super(CancelOrderDAO.TABLE.getName());
    }

    @BeforeEach
    void setUp() {
        new BatchJooqRepository(dsl()).save(new Metadata(1, "hash", "previousHash", 23, false));
    }

    @Test
    void saves_a_cancel_order() {
        CancelOrder cancelOrder = new CancelOrder(123L, 1, "keyid", 56789L, 26L);

        new CancelOrderJooqRepository(dsl()).save(singletonList(cancelOrder));

        List<CancelOrder> cancelOrders = dsl().select(ID, BATCH_ID, KEY_ID, CREATION_DATE, ORDER_TO_CANCEL).from(CancelOrderDAO.TABLE).fetchInto(CancelOrder.class);
        assertThat(cancelOrders).hasSize(1);
        assertThat(cancelOrders.get(0)).isEqualToComparingFieldByField(cancelOrder);
    }
}