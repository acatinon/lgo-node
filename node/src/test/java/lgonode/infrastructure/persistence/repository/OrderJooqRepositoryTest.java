package lgonode.infrastructure.persistence.repository;

import lgonode.domain.Metadata;
import lgonode.domain.Order;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.OrderDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static lgonode.infrastructure.persistence.dao.OrderDAO.*;
import static org.assertj.core.api.Assertions.assertThat;

class OrderJooqRepositoryTest extends JooqTestCase {

    public OrderJooqRepositoryTest() {
        super(OrderDAO.TABLE.getName());
    }

    @BeforeEach
    void setUp() {
        new BatchJooqRepository(dsl()).save(new Metadata(1, "hash", "previousHash", 23, false));
    }

    @Test
    void saves_orders() {
        Order order1 = new Order(123L, 1, "keyid", 56789L, "L", "S", "BTC", "USD", "4.35000000", "3200.00");
        Order order2 = new Order(124L, 1, "keyid", 56790L, "M", "B", "BTC", "USD", "4.35000000", null);

        new OrderJooqRepository(dsl()).save(Arrays.asList(order1, order2));

        List<Order> savedOrders = dsl().select(ID, BATCH_ID, KEY_ID, CREATION_DATE, TYPE, DIRECTION, BASE_CURRENCY, QUOTE_CURRENCY, QUANTITY, PRICE).from(OrderDAO.TABLE).orderBy(OrderDAO.ID).fetchInto(Order.class);
        assertThat(savedOrders).hasSize(2);
        assertThat(savedOrders.get(0)).isEqualToComparingFieldByField(order1);
        assertThat(savedOrders.get(1)).isEqualToComparingFieldByField(order2);
    }
}