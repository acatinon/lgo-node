package lgonode.infrastructure.persistence.repository;

import lgonode.domain.Metadata;
import lgonode.infrastructure.persistence.JooqTestCase;
import lgonode.infrastructure.persistence.dao.BatchDAO;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class BatchJooqRepositoryTest extends JooqTestCase {

    public BatchJooqRepositoryTest() {
        super(BatchDAO.TABLE.getName());
    }

    @Test
    void saves_batch_metadata() {
        Metadata metadata = new Metadata(123L, "hash", "previousHash", 23, true);

        new BatchJooqRepository(dsl()).save(metadata);

        List<Metadata> batchList = dsl().selectFrom(BatchDAO.TABLE).fetchInto(Metadata.class);
        assertThat(batchList).hasSize(1);
        assertThat(batchList.get(0)).isEqualToComparingFieldByField(metadata);
    }

    @Test
    void returns_the_last_batch() {
        BatchJooqRepository batchJooqRepository = new BatchJooqRepository(dsl());
        batchJooqRepository.save(new Metadata(124L, "hash", "previousHash", 23, false));
        batchJooqRepository.save(new Metadata(125L, "hash", "previousHash", 23, false));
        batchJooqRepository.save(new Metadata(126L, "hash", "previousHash", 23, false));

        Metadata metadata = batchJooqRepository.lastBatch();

        assertThat(metadata).isEqualToComparingFieldByField(new Metadata(126L, "hash", "previousHash", 23, false));
    }

    @Test
    void returns_null_when_no_batch() {
        BatchJooqRepository batchJooqRepository = new BatchJooqRepository(dsl());

        Metadata metadata = batchJooqRepository.lastBatch();

        assertThat(metadata).isNull();
    }

}