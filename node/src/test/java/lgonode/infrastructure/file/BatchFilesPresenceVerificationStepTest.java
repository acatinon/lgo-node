package lgonode.infrastructure.file;

import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.ProcessingResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;

class BatchFilesPresenceVerificationStepTest {

    @BeforeEach
    void setUp() {
        Path path = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        step = new BatchFilesPresenceVerificationStep(path.toString());
    }

    @Test
    void it_validates_full_directory() {
        BatchProcessingContext context = BatchProcessingContext.create("0000000000", "lastBatchHash");
        ProcessingResult process = step.process(context);

        assertThat(process).isEqualTo(ProcessingResult.CONTINUE);
    }

    @Test
    void it_does_not_validate_partial_directory() {
        BatchProcessingContext context = BatchProcessingContext.create("0002840150", "lastBatchHash");
        ProcessingResult process = step.process(context);

        assertThat(process).isEqualTo(ProcessingResult.WAIT);
    }

    @Test
    void it_does_not_validate_not_existing_directory() {
        BatchProcessingContext context = BatchProcessingContext.create("0010020005", "lastBatchHash");
        ProcessingResult process = step.process(context);

        assertThat(process).isEqualTo(ProcessingResult.WAIT);
    }

    private BatchFilesPresenceVerificationStep step;
}