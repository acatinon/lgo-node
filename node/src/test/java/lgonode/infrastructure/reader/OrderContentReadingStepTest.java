package lgonode.infrastructure.reader;

import lgonode.domain.CancelOrder;
import lgonode.domain.Order;
import lgonode.infrastructure.BatchProcessingContext;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OrderContentReadingStepTest {

    @Test
    void it_reads_order() {
        String orderData = "L,B,BTC-USD,7.7855,26.4,gtc,1552488758";
        OrderContentReadingStep orderReader = new OrderContentReadingStep();
        BatchProcessingContext context = BatchProcessingContext.create("000001", "lastBatchHash").orderDecrypted(12L, "keyid", orderData);

        orderReader.process(context);

        assertThat(context.orders()).hasSize(1);
        Order order = context.orders().get(0);
        assertThat(order.id).isEqualTo(12L);
        assertThat(order.type).isEqualTo("L");
        assertThat(order.direction).isEqualTo("B");
        assertThat(order.baseCurrency).isEqualTo("BTC");
        assertThat(order.quoteCurrency).isEqualTo("USD");
        assertThat(order.quantity).isEqualTo("7.7855");
        assertThat(order.price).isEqualTo("26.4");
        assertThat(order.batchId).isEqualTo(1);
        assertThat(order.creationDate).isEqualTo(1552488758000L);
    }

    @Test
    void it_reads_cancel_order() {
        String orderData = "C,12345,1552488758";
        OrderContentReadingStep orderReader = new OrderContentReadingStep();
        BatchProcessingContext context = BatchProcessingContext.create("000001", "lastBatchHash").orderDecrypted(12L, "keyid", orderData);

        orderReader.process(context);

        assertThat(context.cancelOrders()).hasSize(1);
        CancelOrder cancelOrder = (CancelOrder) context.cancelOrders().get(0);
        assertThat(cancelOrder.id).isEqualTo(12L);
        assertThat(cancelOrder.batchId).isEqualTo(1);
        assertThat(cancelOrder.orderIdToCancel).isEqualTo(12345L);
        assertThat(cancelOrder.creationDate).isEqualTo(1552488758000L);
    }
}