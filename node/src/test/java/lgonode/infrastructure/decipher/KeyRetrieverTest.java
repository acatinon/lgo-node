package lgonode.infrastructure.decipher;

import com.google.api.client.util.Lists;
import lgonode.infrastructure.gcs.GCSKeyBucketReader;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.security.Security;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class KeyRetrieverTest {

    @BeforeEach
    void setUp() throws FileNotFoundException {
        Security.addProvider(new BouncyCastleProvider());
        Path keyDirectory = new File(getClass().getClassLoader().getResource("files/key").getFile()).toPath();
        bucketReader = mock(GCSKeyBucketReader.class);
        fileReader = new FileReader(keyDirectory.resolve("618bbc95-b6d4-4a24-8967-bfa8e83367b1_private.pem").toFile());
        when(bucketReader.loadPrivateKey(any())).thenReturn(Optional.of(fileReader));
        keyRetriever = new KeyRetriever(bucketReader);
    }

    @Test
    void calls_gcs_if_not_in_cache_and_retrieves_private_key() throws IOException {
        keyRetriever.retrievePrivateKeys(keyIds());

        verify(bucketReader).loadPrivateKey(KEY_ID);
        fileReader.close();
        assertThat(keyRetriever.getPrivateKey(KEY_ID)).isPresent();
        assertThat(keyRetriever.getPrivateKey(KEY_ID).get().getAlgorithm()).isEqualTo("RSA");
    }

    @Test
    void does_take_key_in_cache() {
        keyRetriever.retrievePrivateKeys(keyIds());
        reset(bucketReader);

        keyRetriever.retrievePrivateKeys(keyIds());

        verifyZeroInteractions(bucketReader);
    }

    private List<String> keyIds() {
        List<String> keyIds = Lists.newArrayList();
        keyIds.add(KEY_ID);
        return keyIds;
    }

    private static final String KEY_ID = "618bbc95-b6d4-4a24-8967-bfa8e83367b1";
    private KeyRetriever keyRetriever;
    private GCSKeyBucketReader bucketReader;
    private FileReader fileReader;
}