package lgonode.infrastructure.decipher;

import lgonode.domain.DecryptedOrder;
import lgonode.infrastructure.BatchProcessingContext;
import lgonode.infrastructure.avro.BatchFilesReadingStep;
import lgonode.infrastructure.gcs.GCSKeyBucketReader;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.security.Security;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OrderDecryptionStepTest {

    @BeforeEach
    void setUp() throws FileNotFoundException {
        Security.addProvider(new BouncyCastleProvider());
        Path pathBatch = new File(getClass().getClassLoader().getResource("files/batch").getFile()).toPath();
        context = BatchProcessingContext.create("0002020005", "lastBatchHash");
        new BatchFilesReadingStep(pathBatch.toString()).process(context);
        Path path = new File(getClass().getClassLoader().getResource("files/key").getFile()).toPath();

        GCSKeyBucketReader gcsKeyBucketReader = mock(GCSKeyBucketReader.class);
        fileReader = new FileReader(path.resolve("618bbc95-b6d4-4a24-8967-bfa8e83367b1_private.pem").toFile());
        when(gcsKeyBucketReader.loadPrivateKey("618bbc95-b6d4-4a24-8967-bfa8e83367b1")).thenReturn(Optional.of(fileReader));
        orderDecipherer = new OrderDecryptionStep(new KeyRetriever(gcsKeyBucketReader));
    }

    @AfterEach
    void tearDown() throws IOException {
        fileReader.close();
    }

    @Test
    void it_processes() {
        orderDecipherer.process(context);

        Optional<DecryptedOrder> order = context.decryptedOrders().stream().filter(o -> o.id == 155248875816700001L).findFirst();
        assertThat(order.isPresent()).isTrue();
        DecryptedOrder decryptedOrder = order.get();
        assertThat(decryptedOrder.data).isEqualTo("L,B,BTC-USD,7.7855,26.4,gtc,1552488758");
        assertThat(decryptedOrder.id).isEqualTo(155248875816700001L);
    }

    private OrderDecryptionStep orderDecipherer;
    private BatchProcessingContext context;

    private FileReader fileReader;
}